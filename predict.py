import argparse
import logging
import time
import cv2
import os
import numpy as np
import faiss

import tensorflow as tf
from moviepy.editor import VideoFileClip

from pipeline.pipeline import DetectAndTrack
from tracking.kalman_tracker import KalmanTracker
from extraction.extractor import Extractor
import utils.drawing

gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

res = faiss.StandardGpuResources()  # use a single GPU



'''
    Purpose: Matching trackers in 2 detec_and_track Object
    Arguments:
        img_p, img_g: RGB input image of 2 cameras
        detect_and_track_p, detect_and_track_g: detec_and_track Object of 2 cameras
'''
def pipepline(img_p, img_g, detect_and_track_p, detect_and_track_g):
    threshold_reid = 0.55
    output_image_p = detect_and_track_p.pipeline(img_p) # Probe tracker
    output_image_g = detect_and_track_g.pipeline(img_g) # Gallery tracker

    tracker_list_p = detect_and_track_p.tracker_list
    tracker_list_g = detect_and_track_g.tracker_list

    gallery_database = []
    gallery_tracking_id = []
    gallery_tracker_idx = []
    if len(tracker_list_g)>0:
        for trk_idx in range(len(tracker_list_g)):
            if not tracker_list_g[trk_idx].matched:
                for unit_object in tracker_list_g[trk_idx].unit_object:
                    gallery_database.append(unit_object.feature.cpu().numpy())
                    gallery_tracking_id.append(tracker_list_g[trk_idx].tracking_id)
                    gallery_tracker_idx.append(trk_idx) 
    print(gallery_tracking_id)

    if len(tracker_list_p)>0 and len(gallery_database)>0:
        for trk_idx in range(len(tracker_list_p)):
            if tracker_list_p[trk_idx].other_id is None:
                # Add database into faiss
                index_flat = faiss.IndexFlatL2(2048)
                gpu_index_flat = index_flat
                # print(np.array(gallery_database).shape)
                gpu_index_flat.add(np.array(gallery_database).reshape((-1,2048))) 
            
                # Searching            
                for unit_object in tracker_list_p[trk_idx].unit_object:
                    input_feat = unit_object.feature.cpu().numpy()
                    D, I = gpu_index_flat.search(input_feat, k=1)
                    if D[0][0] < threshold_reid :
                        tracker_list_p[trk_idx].other_id = gallery_tracking_id[I[0][0]]
                        tracker_list_g[gallery_tracker_idx[I[0][0]]].matched = True
                        break     
            else:
                continue

    good_tracker_list = []
    for trk in tracker_list_p:
        if (trk.hits >= 1) and (trk.no_losses <= 4):
            good_tracker_list.append(trk)
            img_p = utils.drawing.draw_box_label(img_p, trk, 'car')
    good_tracker_list = []
    for trk in tracker_list_g:
        if (trk.hits >= 1) and (trk.no_losses <= 4):
            good_tracker_list.append(trk)
            img_g = utils.drawing.draw_box_label(img_g, trk, 'car')
    
    combine_image = np.concatenate([img_g, img_p], axis=1)
    cv2.imwrite('output/1.jpg', combine_image) 


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Detect and Track')
    parser.add_argument("--input_g", dest="input_g",
                        help="path for video file",
                        default="input/inter4")
    parser.add_argument("--input_p", dest="input_p",
                        help="path for video file",
                        default="input/inter4_cut")
    parser.add_argument("--output", dest="output_file",
                        help="path for processed video file",
                        default="output.mp4")
    parser.add_argument("--detector", dest='detector',
                        help="mobilenet or yolov3",
                        default="yolov4", type=str)
    parser.add_argument("--type_extractor", default=0, help="0: reid; 1: reid+camif+orient0")
    parsed_args = parser.parse_args()

    assert parsed_args.detector.lower() in ["mobilenet", "yolov3", "yolov4"]

    if parsed_args.detector.lower() == "mobilenet":
        from detection.mobilenet_ssd_detector import Detector
        detector = Detector()
    elif parsed_args.detector.lower() == "yolov3":
        from detection.yolo_v3_detector import Detector
        detector = Detector()
    elif parsed_args.detector.lower() == "yolov4":
        from detection.yolo_v4_detector import Detector
        detector = Detector()

    if parsed_args.type_extractor == 0:
        extractor = Extractor(type_extractor='reid')
    else:
        extractor = Extractor(type_extractor='all')

    detect_and_track_p = DetectAndTrack(detector, extractor, KalmanTracker)
    detect_and_track_g = DetectAndTrack(detector, extractor, KalmanTracker)

    list_name_image = sorted(os.listdir(parsed_args.input_g))
    for name_image in list_name_image:
        start = time.time()
        path_gallery = os.path.join(parsed_args.input_g, name_image)
        path_probe = os.path.join(parsed_args.input_p, name_image)

        img_gallery = cv2.imread(path_gallery)
        img_probe = cv2.imread(path_probe)
        pipepline(img_probe, img_gallery, detect_and_track_p, detect_and_track_g)
        print(time.time()-start)
