import argparse
import logging
import time
import cv2
import os

import tensorflow as tf
from moviepy.editor import VideoFileClip

from pipeline.pipeline import DetectAndTrack
from tracking.kalman_tracker import KalmanTracker
from extraction.extractor import Extractor

gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Detect and Track')
    parser.add_argument("--input", dest="input_file",
                        help="path for video file",
                        default="video.mp4")
    parser.add_argument("--output", dest="output_file",
                        help="path for processed video file",
                        default="output.mp4")
    parser.add_argument("--detector", dest='detector',
                        help="mobilenet or yolov3",
                        default="yolov4", type=str)
    parser.add_argument("--type_extractor", default=0, help="0: reid; 1: reid+camif+orient0")
    parsed_args = parser.parse_args()

    assert parsed_args.detector.lower() in ["mobilenet", "yolov3", "yolov4"]

    if parsed_args.detector.lower() == "mobilenet":
        from detection.mobilenet_ssd_detector import Detector
        detector = Detector()
    elif parsed_args.detector.lower() == "yolov3":
        from detection.yolo_v3_detector import Detector
        detector = Detector()
    elif parsed_args.detector.lower() == "yolov4":
        from detection.yolo_v4_detector import Detector
        detector = Detector()

    if parsed_args.type_extractor == 0:
        extractor = Extractor(type_extractor='reid')
    else:
        extractor = Extractor(type_extractor='all')

    detect_and_track = DetectAndTrack(detector, extractor, KalmanTracker)
    vid = cv2.VideoCapture('/home/nguyenpc/project/red_light/Detect-and-Track/input/inter4_cut.mp4')
    count_frame = 0
    while True:
        return_value, frame = vid.read()
        if return_value:
            count_frame+=1
            start = time.time()
            # image = detect_and_track.pipeline(frame)
       
            print(time.time()-start)
            cv2.imwrite('input/inter4_cut/%04d.jpg'%count_frame, frame) 
    # list_name_image = os.listdir('/home/nguyenpc/project/red_light/Detect-and-Track/input/inter3')
    # for name_image in sorted(list_name_image):
    #     path_gallery = os.path.join('/home/nguyenpc/project/red_light/Detect-and-Track/input/inter3', name_image)

    #     img_gallery = cv2.imread(path_gallery)
    #     # bgr_img_probe = cv2.cvtColor(img_probe, cv2.COLOR_RGB2BGR)
        
    #     output_image1 = detect_and_track.without_tracking(img_gallery, True)
    #     cv2.imwrite('output/1.jpg', output_image1) 
'''
    ---> There are a list of tracker (tracker_list)
    ---> Extract feature for all
    ---> Matching cameras's view
'''