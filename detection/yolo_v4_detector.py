# import logging
# import os
from typing import List

# import torch
# from torch.autograd import Variable

from pipeline import UnitObject
from detection.base_detector import BaseDetector
# from detection.yolov3.clone.yolov3.darknet import Darknet
# from detection.yolov3.clone.yolov3.preprocess import prep_frame
# from detection.yolov3.clone.yolov3.util import load_classes, write_results

import os, json
import numpy as np
from PIL import Image

import tensorflow as tf 
from tensorflow.keras.models import model_from_json
from tensorflow.keras.layers import Layer
from tensorflow.keras import backend as K

import torch
import torchvision.transforms as T

from detection.util_yolo_v4 import Decode
from extraction.baseline import Baseline

class Mish(Layer):
    def __init__(self, **kwargs):
        super(Mish, self).__init__(**kwargs)
        self.supports_masking = True

    def call(self, inputs):
        return inputs * K.tanh(K.softplus(inputs))

    def get_config(self):
        config = super(Mish, self).get_config()
        return config

    def compute_output_shape(self, input_shape):
        return input_shape

class Detector(BaseDetector):

    def __init__(self):
        super().__init__()
        self.conf_thresh = 0.5
        self.nms_thresh = 0.45
        self.model_image_size = (416, 416)
        self.model_json = '/home/nguyenpc/project/red_light/keras-yolo4/model_data/custom_model_wo_mo.json'
        self.model_weight = '/home/nguyenpc/project/red_light/keras-yolo4/model_data/custom_model_wo_mo.h5'
        self._load_detector()
        self.class_names = ['car', 'bus', 'truck']
        self._decode = Decode(self.conf_thresh, self.nms_thresh, self.model_image_size,\
                         self.model, self.class_names)
        

    def _load_detector(self):
        with open(self.model_json, 'r') as json_file:
            loaded_model_json = json.load(json_file)
        model = model_from_json(loaded_model_json, custom_objects={'Mish':Mish})
        model.load_weights(self.model_weight)
        self.model = model


    def get_detections(self, image, draw=False) -> List[UnitObject]:
        image, boxes, scores, classes = self._decode.detect_image(image.copy(), draw)

        ret = []
        if boxes is not None:
            for box, cl in zip(boxes, classes):
                x0, y0, x1, y1 = box
                left = max(0, np.floor(x0 + 0.5).astype(int))
                top = max(0, np.floor(y0 + 0.5).astype(int))
                right = min(image.shape[1], np.floor(x1 + 0.5).astype(int))
                bottom = min(image.shape[0], np.floor(y1 + 0.5).astype(int))
                ret.append(UnitObject([top, left, bottom, right], cl, None))
        return ret, image