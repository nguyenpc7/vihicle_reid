import os 
import torch
from PIL import Image
import torchvision.transforms as T

from extraction.config import cfg
from extraction.baseline import Baseline


class Extractor():
    def __init__(self, type_extractor='reid'):
        self.reid_weight='/home/nguyenpc/project/red_light/AICity2020-VOC-ReID/output/aicity20/0409-ensemble/r50-320-circle/best.pth'
        self.camid_weight='/home/nguyenpc/project/red_light/AICity2020-VOC-ReID/output/aicity20/0409-ensemble/ReCamID/best.pth'
        self.orient_weight='/home/nguyenpc/project/red_light/AICity2020-VOC-ReID/output/aicity20/0409-ensemble/ReOriID/best.pth'
        self.device = 'cuda'
        self.type_extractor = type_extractor
        self.get_cfg()
        self._load_extractor()
        
    @staticmethod
    def get_cfg():
        cfg.merge_from_file('extraction/config/aicity20.yml')
        cfg.freeze()
        return cfg

    def _load_extractor(self):
        cfg = self.get_cfg()
        model_reid = Baseline(333, 1, None, 'bnneck',
                         'after', 'resnet50_ibn_a', 'self',
                         cfg)
        model_reid.load_param(self.reid_weight)

        model_camid = Baseline(333, 1, None, 'bnneck',
                         'after', 'resnet50_ibn_a', 'self',
                         cfg)
        model_camid.load_param(self.camid_weight)

        model_orient = Baseline(333, 1, None, 'bnneck',
                         'after', 'resnet50_ibn_a', 'self',
                         cfg)
        model_orient.load_param(self.orient_weight) 

        model_reid.to(self.device)
        model_reid.eval()
        model_camid.to(self.device)
        model_camid.eval()
        model_orient.to(self.device)
        model_orient.eval() 
        self.model_reid = model_reid
        self.model_camid = model_camid
        self.model_orient = model_orient
    
    def get_extraction(self, frame, unit_detections):
        transform = T.Compose([T.Resize([256,256]),
                                T.ToTensor()])   
        

        with torch.no_grad():
            if self.type_extractor == 'reid': # Only reid model
                if len(unit_detections) !=0:
                    for det_idx in range(len(unit_detections)):
                        top, left, bottom, right = unit_detections[det_idx].box
                        croped_image = Image.fromarray(frame[top:bottom, left:right])
                        croped_image = transform(croped_image).float()
                        data = croped_image.reshape((1,3,256,256)).cuda()

                        feat_reid = self.model_reid(data)
                        normed_feature = torch.nn.functional.normalize(feat_reid, dim=1, p=2)
                        unit_detections[det_idx].feature = normed_feature
            else: # reid+camid+orient
                if len(unit_detections) !=0:
                    for det_idx in range(len(unit_detections)):
                        top, left, bottom, right = unit_detections[det_idx].box
                        croped_image = Image.fromarray(frame[top:bottom, left:right])
                        croped_image = transform(croped_image).float()
                        data = croped_image.reshape((1,3,256,256)).cuda()
                        
                        feat_reid = self.model_reid(data)
                        feat_camid = self.model_camid(data)
                        feat_orient = self.model_orient(data)
                        
                        feature = feat_reid - 0.1*feat_camid - 0.1*feat_orient
                        normed_feature = torch.nn.functional.normalize(feature, dim=1, p=2)
                        unit_detections[det_idx].feature = normed_feature
        
        return unit_detections
