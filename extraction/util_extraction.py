import os
import argparse
from extraction.config import cfg

def get_cfg():
    cfg.merge_from_file('extraction/config/aicity20.yml')
    cfg.freeze()
    return cfg