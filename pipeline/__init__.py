from typing import List


class UnitObject:
    def __init__(self, box=[], idx=None, feature=None):
        """
        Create bounding box with id of vector
        :param bounds: [top, left, bottom, right]
        :param id: type of classification
        """
        self.box: List[int] = box
        self.class_id: int = idx
        self.feature = feature

    def __str__(self):
        return str(self.class_id) + ": " + str(self.box)