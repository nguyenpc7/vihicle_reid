# Vihicle Reid

#### Detection: Yolov4-backbone Darknet
> Input: 416,416,3 - BGR image (cv2.imread)

> Output: Feature map 52x52, 26x26, 13x16

> Anchor boxes: [[12, 16], [19, 36], [40, 28], [36, 75], [76, 55], [72, 146], [142, 110], [192, 243], [459, 401]]

#### Tracker: Kalman filter
> hits: 1
> ages: 4
> return trackers for each view

#### Reid: VOCreid-backbone Resnet50 with triplet loss
> Input: 256x256x3 - RGB image (croped from dectector)

> Output: 1x2048 

> Matching: Cosin distance

## Abation study
> Extract feature and match tracklet frame by frame

> Probe tracklet search in gallery tracklet:
> - If in other_id_probe = id_gallery and matched_gallery = True (if of this tracklet out of gallery set). Do not continuously search the id_probe.
> - Esle continuously search the id_probe

## True case:
> Vihicle is correctly detected and stably tracked (a lit of bit occluded)

## False case:
> - Detector is not stable (with some new kind of vihicle) --> re-train model with new data
> - Tracker is not stable (occluded vihicle) --> using trajector of vihicle
> - Reid is confused (same color) --> using location information